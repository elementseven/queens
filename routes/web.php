<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Page Routes
Route::get('/', [App\Http\Controllers\PageController::class, 'home'])->name('home');
Route::get('/about', [App\Http\Controllers\PageController::class, 'about'])->name('about');
Route::get('/contact', [App\Http\Controllers\PageController::class, 'contact'])->name('contact');
Route::get('/terms-and-conditions', [App\Http\Controllers\PageController::class, 'tandcs'])->name('tandcs');
Route::get('/privacy-policy', [App\Http\Controllers\PageController::class, 'privacyPolicy'])->name('privacy-policy');

// News Routes
Route::get('/news', [App\Http\Controllers\NewsController::class, 'index'])->name('news.index');
Route::get('/news/get', [App\Http\Controllers\NewsController::class, 'get'])->name('news.get');
Route::get('/news/{date}/{slug}', [App\Http\Controllers\NewsController::class, 'show'])->name('news.show');


// Support Routes
Route::get('/support', [App\Http\Controllers\SupportController::class, 'index'])->name('support.index');
Route::get('/support/get', [App\Http\Controllers\SupportController::class, 'get'])->name('support.get');
Route::get('/support/{date}/{slug}', [App\Http\Controllers\SupportController::class, 'show'])->name('support.show');


// // Blog Routes
// Route::get('/blog', [App\Http\Controllers\PublicBlogController::class, 'index'])->name('blog.index');
// Route::get('/blog/get', [App\Http\Controllers\PublicBlogController::class, 'get'])->name('blog.get');
// Route::get('/blog/{date}/{slug}', [App\Http\Controllers\PublicBlogController::class, 'show'])->name('blog.show');

// Event Routes
Route::get('/events', [App\Http\Controllers\EventController::class, 'index'])->name('events.index');
Route::get('/events/get', [App\Http\Controllers\EventController::class, 'get'])->name('events.get');
Route::get('/events/{date}/{slug}', [App\Http\Controllers\EventController::class, 'show'])->name('events.show');

// Case Studies Routes
Route::get('/case-studies', [App\Http\Controllers\CaseStudyController::class, 'index'])->name('case-studies.index');
Route::get('/case-studies/get', [App\Http\Controllers\CaseStudyController::class, 'get'])->name('case-studies.get');
Route::get('/case-studies/{casestudy}/{slug}', [App\Http\Controllers\CaseStudyController::class, 'show'])->name('case-studies.show');

// Resources Routes
Route::get('/resource-hub', [App\Http\Controllers\ResourceController::class, 'index'])->name('resources.index');
Route::get('/resource-hub/get', [App\Http\Controllers\ResourceController::class, 'get'])->name('resources.get');
Route::get('/resource-hub/{document}/{slug}', [App\Http\Controllers\ResourceController::class, 'download'])->name('resources.download');

// Mailing Routes
Route::post('/send-message', [App\Http\Controllers\SendMail::class, 'enquiry'])->name('send-message');
Route::post('/mailinglist', [App\Http\Controllers\SendMail::class, 'mailingListSignup'])->name('mailing-list');

// Route::get('login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
// Route::post('login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
// Route::post('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

// Route::get('register', [App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
// Route::post('register', [App\Http\Controllers\Auth\RegisterController::class, 'register']);

Route::get('password/reset', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('password/email', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [App\Http\Controllers\Auth\ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('password/reset', [App\Http\Controllers\Auth\ResetPasswordController::class, 'reset'])->name('password.update');

Route::get('password/confirm', [App\Http\Controllers\Auth\ConfirmPasswordController::class, 'showConfirmForm'])->name('password.confirm');
Route::post('password/confirm', [App\Http\Controllers\Auth\ConfirmPasswordController::class, 'confirm']);

Route::get('email/verify', [App\Http\Controllers\Auth\VerificationController::class, 'show'])->name('verification.notice');
Route::get('email/verify/{id}/{hash}', [App\Http\Controllers\Auth\VerificationController::class, 'verify'])->name('verification.verify');
Route::post('email/resend', [App\Http\Controllers\Auth\VerificationController::class, 'resend'])->name('verification.resend');

Route::get('/get-involved', [App\Http\Controllers\PageController::class, 'getInvolved'])->name('get-involved');
Route::post('/submit-get-involved', [App\Http\Controllers\SendMail::class, 'getInvolvedForm'])->name('submit-get-involved');

// Members area
Route::get('/members/private-blog', [App\Http\Controllers\PrivateBlogController::class, 'index'])->name('members.private-blog')->middleware('verified');
Route::get('/members/private-blog/get', [App\Http\Controllers\PrivateBlogController::class, 'get'])->name('members.private-blog.get')->middleware('verified');
Route::get('/members/private-blog/{date}/{slug}', [App\Http\Controllers\PrivateBlogController::class, 'show'])->name('members.private-blog.show')->middleware('verified');

Route::get('/members/account', [App\Http\Controllers\UserController::class, 'account'])->name('members.account')->middleware('verified');
Route::post('/update-account', [App\Http\Controllers\UserController::class, 'update'])->name('update-account')->middleware('verified');
Route::post('/change-password', [App\Http\Controllers\UserController::class, 'changePassword'])->name('change-password')->middleware('verified');
Route::post('/remove-account', [App\Http\Controllers\UserController::class, 'destroy'])->name('remove-account')->middleware('verified');

Route::get('/logout', function(){
    Auth::logout();
    return redirect()->to('/login');
})->name('logout');

Auth::routes();
