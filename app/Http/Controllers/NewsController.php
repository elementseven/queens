<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use Auth;

class NewsController extends Controller
{

    // Main news page
  public function index(){
    $categories = Category::has('posts')->orderBy('name','asc')->get();
    return view('news.index')->with(['categories' => $categories]);
  }

  // Return json news posts
  public function get(Request $request){
    $search = $request->input('search');
    if($request->input('category') == '*'){
      $posts = Post::where('status','!=','draft')
      ->when($search != "", function($q) use ($search) {
        $q->where('title','LIKE', '%'.$search.'%');
      })
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }else{
      $posts = Post::orderBy('created_at','desc')->whereHas('categories', function($q) use($request){
          $q->where('id', $request->input('category'));
      })
      ->when($search != "", function($q) use ($search) {
        $q->where('title','LIKE', '%'.$search.'%');
      })
      ->where('status','!=','draft')
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }
    foreach($posts as $p){
      $p->normal = $p->getFirstMediaUrl('news', 'normal');
      $p->normalwebp = $p->getFirstMediaUrl('news', 'normal-webp');
      $p->mimetype = $p->getFirstMedia('news')->mime_type;
    }
    return $posts;
  }

  // Single news article
  public function show($date, $slug){
    if(Auth::user() && Auth::user()->role->slug =='admin'){
      $post = Post::where([['slug',$slug]])->whereDate('created_at', $date)->first();
      $others = Post::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(3);
    }else{
      $post = Post::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
      if(isset($post)){
        $others = Post::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(3);
      }
    }
    return view('news.show')->with(['post' => $post, 'others' => $others]);
  }

}
