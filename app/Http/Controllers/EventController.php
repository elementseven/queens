<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('events.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request){

        $search = $request->input('search');

        $events = Event::where('status','!=','draft')
        ->when($search != "", function($q) use ($search) {
            $q->where('title','LIKE', '%'.$search.'%');
        })
        ->orderBy('datetime','desc')
        ->paginate($request->input('limit'),['id','title', 'slug', 'datetime', 'excerpt', 'location']);

        return $events;
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($date, $slug){
        if(Auth::user() && Auth::user()->role->slug =='admin'){
            $event = Event::where('slug',$slug)->whereDate('datetime', $date)->first();
            $others = Event::where('id','!=', $event->id)->where('status','!=','draft')->orderBy('datetime','desc')->paginate(3);
        }else{
            $event = Event::where([['slug',$slug],['status','!=','draft']])->whereDate('datetime', $date)->first();
            $others = Event::where('id','!=', $event->id)->where('status','!=','draft')->orderBy('datetime','desc')->paginate(3);
        }
        return view('events.show')->with(['event' => $event, 'others' => $others]);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
