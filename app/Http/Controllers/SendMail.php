<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

use Newsletter;
use Mail;
use Auth;


class SendMail extends Controller
{
  public function enquiry(Request $request){

    // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 

        $subject = "General Enquiry";
        $name = $request->input('name');
        $email = $request->input('email');
        $content = $request->input('message');
        $phone = null;
        if($request->input('phone') != ""){
            $phone = $request->input('phone');
        }
        Mail::send('emails.enquiry',[
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'phone' => $phone,
            'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $phone){
            $message->from('donotreply@lorem.com', 'QUB PPI Hub');
            $message->subject($subject);
            $message->replyTo($email);
            $message->to('ppihub@qub.ac.uk');
            //$message->to('luke@elementseven.co');
        });
        return 'success';
    }

    public function getInvolvedForm(Request $request){

    // Validate the form data
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'email' => 'required|email',
            'short_description' => 'required|string',
        ]); 

        $subject = "Get Involved Enquiry";
        $name = $request->input('name');
        $email = $request->input('email');
        $shortdescription = $request->input('short_description');

        $lead = null;
        if($request->input('lead') != ""){
            $lead = $request->input('lead');
        }
        $contact = null;
        if($request->input('contact') != ""){
            $contact = $request->input('contact');
        }

        Mail::send('emails.get-involved',[
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'lead' => $lead,
            'contact' => $contact,
            'shortdescription' => $shortdescription
        ], function ($message) use ($subject, $email, $name, $contact, $lead, $shortdescription){
            $message->from('donotreply@lorem.com', 'QUB PPI Hub');
            $message->subject($subject);
            $message->replyTo($email);
            $message->to('ppihub@qub.ac.uk');
            //$message->to('luke@elementseven.co');
        });
        return 'success';
    }

    public function mailingListSignup(Request $request){
        $this->validate($request,[
          'email' => 'required|string|email|max:255'
        ]);
        Newsletter::subscribe($request->input('email'));
        return "success";
    }
}