<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CaseStudy;

class CaseStudyController extends Controller
{
    /**
     * Display the main case studies page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('case-studies.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request){
        $search = $request->input('search');

        $casestudies = CaseStudy::where('status','!=','draft')
        ->when($search != "", function($q) use ($search) {
            $q->where('title','LIKE', '%'.$search.'%');
        })
        ->orderBy('created_at','desc')
        ->paginate($request->input('limit'),['id','title', 'slug', 'created_at']);
       
      foreach($casestudies as $cs){
          $cs->normal = $cs->getFirstMediaUrl('case_studies', 'normal');
          $cs->normalwebp = $cs->getFirstMediaUrl('case_studies', 'normal-webp');
          $cs->mimetype = $cs->getFirstMedia('case_studies')->mime_type;
          $cs->download = $cs->getFirstMediaUrl('case_study_pdfs');
      }
      return $casestudies;
    }

    /**
     * Display a single case study.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(CaseStudy $casestudy, $slug){
        $others = CaseStudy::where('id','!=', $casestudy->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(3);
        return view('case-studies.show')->with(['post' => $casestudy, 'others' => $others]);
    }

    /**
     * Create a future case study
     *
     * @return \Illuminate\Http\Response
     */
    public function submit(Request $request){
        $this->validate($request,[
            'content' => 'required|string'
        ]);
        $updated = array(
            'content' => $request->input('content'),
            'status' => 'Submitted'
        );
        $task->update($updated);
        return response()->json(['success' => 'success', 200]);
    }

}
