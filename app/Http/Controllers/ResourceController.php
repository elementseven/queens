<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('resources.index');
    }

    /**
     * Return an array of the resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $search = $request->input('search');
        if($request->input('category') == '*'){
          $documents = Document::where('status','!=','draft')
          ->when($search != "", function($q) use ($search) {
            $q->where('name','LIKE', '%'.$search.'%');
          })
          ->orderBy('created_at','desc')
          ->with('documentCategory')
          ->paginate($request->input('limit'));
        }else{
          $documents = Document::where('status','!=','draft')
          ->when = Document::orderBy('created_at','desc')->whereHas('documentCategory', function($q) use($request){
              $q->where('id', $request->input('category'));
          })
          ->when($search != "", function($q) use ($search) {
            $q->where('name','LIKE', '%'.$search.'%');
          })
          ->where('status','!=','draft')
          ->orderBy('created_at','desc')
          ->with('documentCategory')
          ->paginate($request->input('limit'));
        }

        return $documents;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download(Document $document)
    {
        $url = $document->getFirstMediaUrl('research-files');
        return redirect()->to($url);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
