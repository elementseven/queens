<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogCat;
use App\Models\PublicBlog;
use Auth;

class PublicBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = BlogCat::has('publicblogs')->orderBy('name','asc')->get();
        return view('blog.index')->with(['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request){

        $search = $request->input('search');

        if($request->input('category') == '*'){

          $posts = PublicBlog::where('status','!=','draft')
          ->when($search != "", function($q) use ($search) {
            $q->where('title','LIKE', '%'.$search.'%');
          })
          ->orderBy('created_at','desc')
          ->with('blogcats')
          ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);

        }else{

          $posts = PublicBlog::orderBy('created_at','desc')->whereHas('blogcats', function($q) use($request){
              $q->where('id', $request->input('category'));
          })
          ->when($search != "", function($q) use ($search) {
            $q->where('title','LIKE', '%'.$search.'%');
          })
          ->where('status','!=','draft')
          ->orderBy('created_at','desc')
          ->with('blogcats')
          ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);

        }

        foreach($posts as $p){
          $p->normal = $p->getFirstMediaUrl('public_blogs', 'normal');
          $p->normalwebp = $p->getFirstMediaUrl('public_blogs', 'normal-webp');
          $p->mimetype = $p->getFirstMedia('public_blogs')->mime_type;
        }

        return $posts;
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($date, $slug){

    if(Auth::user() && Auth::user()->role->slug =='admin'){

      $post = PublicBlog::where([['slug',$slug]])->whereDate('created_at', $date)->first();
      $others = PublicBlog::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(3);

    }else{

      $post = PublicBlog::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
      $others = PublicBlog::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(3);

    }

    return view('blog.show')->with(['post' => $post, 'others' => $others]);

  }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
