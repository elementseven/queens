<?php

namespace App\Http\Controllers;

use App\Models\Support;
use App\Models\Category;
use Illuminate\Http\Request;
use Auth;

class SupportController extends Controller
{
    // Main support page
  public function index(){
    $categories = Category::has('supports')->orderBy('name','asc')->get();
    return view('support.index')->with(['categories' => $categories]);
  }

  // Return json support
  public function get(Request $request){
    $search = $request->input('search');
    if($request->input('category') == '*'){
      $supports = Support::where('status','!=','draft')
      ->when($search != "", function($q) use ($search) {
        $q->where('title','LIKE', '%'.$search.'%');
      })
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }else{
      $supports = Support::orderBy('created_at','desc')->whereHas('categories', function($q) use($request){
          $q->where('id', $request->input('category'));
      })
      ->when($search != "", function($q) use ($search) {
        $q->where('title','LIKE', '%'.$search.'%');
      })
      ->where('status','!=','draft')
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }
    foreach($supports as $s){
      $s->normal = $s->getFirstMediaUrl('support', 'normal');
      $s->normalwebp = $s->getFirstMediaUrl('support', 'normal-webp');
      $s->mimetype = $s->getFirstMedia('support')->mime_type;
    }
    return $supports;
  }

  // Single support article
  public function show($date, $slug){
    if(Auth::user() && Auth::user()->role->slug =='admin'){
      $support = Support::where([['slug',$slug]])->whereDate('created_at', $date)->first();
      $others = Support::where('id','!=', $support->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(3);
    }else{
      $support = Support::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
      if(isset($support)){
        $others = Support::where('id','!=', $support->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(3);
      }
    }
    return view('support.show')->with(['support' => $support, 'others' => $others]);
  }
}
