<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Post;
use Carbon\Carbon;

class PageController extends Controller
{
    public function home(){
        $event = Event::where('status','published')->whereDate('datetime','>=',Carbon::now())->orderBy('datetime', 'asc')->first();
        $post = Post::where('status','published')->orderBy('created_at', 'desc')->first();
        return view('home')->with(['event' => $event, 'post' => $post]);
    }
    public function about(){
        return view('about');
    }
    public function contact(){
        return view('contact');
    }
    public function tandcs(){
        return view('tandcs');
    }
    public function privacyPolicy(){
        return view('privacy-policy');
    }
    public function getInvolved(){
        return view('getInvolved');
    }
}