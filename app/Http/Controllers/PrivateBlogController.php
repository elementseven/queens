<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogCat;
use App\Models\PrivateBlog;

class PrivateBlogController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = BlogCat::has('privateblogs')->orderBy('name','asc')->get();
        return view('members.index')->with(['categories' => $categories]);
    }

    /**
     * Return an array of the resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request){
        $search = $request->input('search');
        if($request->input('category') == '*'){
          $posts = PrivateBlog::where('status','!=','draft')
          ->when($search != "", function($q) use ($search) {
            $q->where('title','LIKE', '%'.$search.'%');
        })
          ->orderBy('created_at','desc')
          ->with('blogcats')
          ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
        }else{
          $posts = PrivateBlog::orderBy('created_at','desc')->whereHas('blogcats', function($q) use($request){
              $q->where('id', $request->input('category'));
          })
          ->when($search != "", function($q) use ($search) {
            $q->where('title','LIKE', '%'.$search.'%');
        })
          ->where('status','!=','draft')
          ->orderBy('created_at','desc')
          ->with('blogcats')
          ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
        }
        foreach($posts as $p){
          $p->normal = $p->getFirstMediaUrl('private_blogs', 'normal');
          $p->normalwebp = $p->getFirstMediaUrl('private_blogs', 'normal-webp');
          $p->mimetype = $p->getFirstMedia('private_blogs')->mime_type;
        }
        return $posts;
    }

    /**
     * Return a single resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($date, $slug){
        $post = PrivateBlog::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
        $others = PrivateBlog::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(3);
        return view('members.show')->with(['post' => $post, 'others' => $others]);
    }
}
