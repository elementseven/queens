<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Info;
use Auth;
use Hash;

class UserController extends Controller
{

    /**
     * Show the account management page.
     */
    public function account()
    {
        return view('members.account');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();

            $this->validate($request,[
                'first_name' => 'required|string',
                'last_name' => 'required|string'
            ]);
            $updated = array(   
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name')
            );
            $user->update($updated);
        
        session(['action_status'=>'Success']);
        session(['action_message'=>'Details updated successfully.']);
        return back();
    }

    public function changePassword(Request $request){
        $user = Auth::user();
        $this->validate($request,[
            'current_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed'
        ]);
        $pwcheck = $this->check($user->password, $request->input('current_password'));
        if($pwcheck == true){
            $updated = array(
                'password' => bcrypt($request->input('password'))
            );
            $user->update($updated);
            session(['action_status'=>'Success']);
            session(['action_message'=>'Password changed successfully.']);
            return back();
        }
        session(['action_status'=>'Error']);
        session(['action_message'=>'The password you entered was incorrect.']);
        return back();
    }

    // Function to check password
    private function check($password, $check){
        if(Hash::check($check, $password)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Auth::user();
        $this->validate($request,[
            'current_password' => 'required|string'
        ]);
        $pwcheck = $this->check($user->password, $request->input('current_password'));
        if($pwcheck == true){
            $user->forceDelete();
        }else{
            session(['action_status'=>'Error']);
            session(['action_message'=>'The password you entered was incorrect.']);
        }
        return back();
    }
}
