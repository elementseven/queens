<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Event;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class UpdateEventDescriptions extends Command
{
    protected $signature = 'update:event-descriptions';
    protected $description = 'Updates event descriptions from Froala to Quill compatible HTML';

    public function handle()
    {
        DB::transaction(function () {
            $events = Event::all(); // Consider using chunking for large data sets

            foreach ($events as $event) {
                $updatedDescription = $this->convertToQuill($event->description);
                $event->update(['description' => $updatedDescription]);
            }
        });

        DB::transaction(function () {
            $posts = Post::all(); // Consider using chunking for large data sets

            foreach ($posts as $post) {
                $updatedDescription = $this->convertToQuill($post->content);
                $post->update(['content' => $updatedDescription]);
            }
        });

        $this->info('Event descriptions updated successfully!');
    }

    

    protected function convertToQuill($html)
    {
        // Load the HTML into a DOMDocument
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        // Remove all style attributes
        $xpath = new \DOMXPath($dom);
        foreach ($xpath->query('//*[@style]') as $node) {
            $node->removeAttribute('style');
        }

        // Remove any specific classes or elements not needed
        foreach ($xpath->query('//div[@data-olk-copy-source="MessageBody"] | //span') as $node) {
            // Convert special characters to HTML entities to avoid issues with createElement
            $nodeValue = htmlspecialchars($node->nodeValue, ENT_QUOTES | ENT_XML1, 'UTF-8');
            $newNode = $dom->createElement('p', $nodeValue);  // Create a <p> node
            $node->parentNode->replaceChild($newNode, $node);  // Replace the current node with <p>
        }

        // Output the cleaned HTML
        $html = $dom->saveHTML();
        $html = strip_tags($html, '<a><p><strong><em><u>');  // Allow only certain tags

        return $html;
    }



}
