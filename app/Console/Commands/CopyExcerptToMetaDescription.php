<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Support; // Adjust this according to your actual namespace

class CopyExcerptToMetaDescription extends Command
{
    protected $signature = 'posts:copy-excerpt-to-meta';
    protected $description = 'Copies all excerpt fields to meta_description fields in the posts table';

    public function handle()
    {
        $posts = Support::all();
        foreach ($posts as $post) {
            $post->meta_description = $post->excerpt;
            $post->save();  // Save each post with the updated meta_description
        }

        $this->info('Excerpts have been successfully copied to meta descriptions.');
    }
}
