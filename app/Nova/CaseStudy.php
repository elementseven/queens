<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Select;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class CaseStudy extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\CaseStudy::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';
    public static $priority = 8;
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Text::make('Title')->sortable(),
            Select::make('Status')->options($statuses),
            Image::make('Image')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('image')->toMediaCollection('case_studies');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('case_studies', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('case_studies', 'thumbnail');
            })->deletable(false)->hideFromIndex(),
            File::make('File')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('file')->toMediaCollection('case_study_pdfs');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('case_study_pdfs', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('case_study_pdfs', 'thumbnail');
            })->storeAs(function(Request $request) {
                return $request->path->getClientOriginalName();
            })->deletable(false)->onlyOnForms(),
            Text::make('Download File', function () {
                return '<a href="'.$this->getFirstMediaUrl('case_study_pdfs').'" target="_blank"><img src="/img/icons/chevron-double-down.svg" width="8" style="display:inline;margin-right:5px;"> Download</a>';
            })->asHtml()->exceptOnForms(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
