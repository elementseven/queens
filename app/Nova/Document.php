<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Str;
class Document extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Document::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static $priority = 4;
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    public static function label() {
        return 'Resources';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Text::make('Name')->displayUsing(function ($text) {
                return Str::limit($text, 60, '...');
            })->sortable(),
            Textarea::make('Excerpt')->nullable(),
            Select::make('Status')->options($statuses)->sortable(),
            File::make('File')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('file')->toMediaCollection('research-files');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('research-files');
                })
                ->deletable(false)->onlyOnForms(),
            Text::make('Link')->nullable()->hideFromIndex(),
            Text::make('Download/Link', function () {
                if($this->link != null){
                    return '<a href="'.$this->link.'" target="_blank"><img src="/img/icons/link-solid.svg" width="16" style="display:inline;margin-right:2px;"> Link</a>';
                }else{
                    return '<a href="'.$this->getFirstMediaUrl('research-files').'" target="_blank"><img src="/img/icons/chevron-double-down.svg" width="8" style="display:inline;margin-right:5px;"> Download</a>';
                }
            })->asHtml()->exceptOnForms(),
            BelongsTo::make('Resource Category', 'documentCategory', 'App\Nova\DocumentCategory')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
