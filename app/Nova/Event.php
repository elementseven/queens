<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Textarea;
use Nevadskiy\Quill\Quill;
use Laravel\Nova\Http\Requests\NovaRequest;
use Carbon\Carbon;
use Illuminate\Support\Str;
class Event extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Event::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Text::make('Title')->displayUsing(function ($text) {
                return Str::limit($text, 90, '...');
            })->required()->sortable(),
            Text::make('Preview', function () {
                return '<a href="/events/'. Carbon::parse($this->datetime)->format('Y-m-d') .'/'. $this->slug.'" target="_blank" class="link-default"> Preview</a>';
            })->asHtml()->exceptOnForms(),
            Text::make('Excerpt')->required()->hideFromIndex(),
            DateTime::make('Date & Time', 'datetime')->required(),
            Text::make('Location')->displayUsing(function ($text) {
                return Str::limit($text, 30, '...');
            })->required(),
            Text::make('Link')->hideFromIndex()->nullable(),
            Select::make('Status')->required()->options($statuses)->sortable(),
            Quill::make('Description')->withFiles()
            ->toolbar([
                [[ 'header' => 2 ]],               
                ['bold', 'italic', 'underline'],
                [['list' => 'ordered'], ['list' => 'bullet']],
                ['blockquote'],
                ['link', 'image', 'video'],
                ['clean'],
            ])->required()->hideFromIndex()->help('Main content of the event page'),
            Textarea::make('Meta Description','meta_description')->onlyOnForms()->nullable(),
            Textarea::make('Keywords','keywords')->onlyOnForms()->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
