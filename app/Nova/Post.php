<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Nevadskiy\Quill\Quill;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Carbon\Carbon;
use Illuminate\Support\Str;
class Post extends Resource
{

    public static function authorizable()
    {
        return true;
    }
    
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Post::class;
    public static $with = ['media'];
    public static $priority = 6;
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    public static function label() {
        return 'News Posts';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Text::make('Title')->displayUsing(function ($text) {
                return Str::limit($text, 90, '...');
            })->required()->sortable(),
            Text::make('Preview', function () {
                return '<a href="/news/'. Carbon::parse($this->created_at)->format('Y-m-d') .'/'. $this->slug.'" target="_blank" class="link-default"> Preview</a>';
            })->asHtml()->exceptOnForms(),
            Date::make('Date', 'created_at')->onlyOnIndex(),
            Text::make('Excerpt')->required()->sortable()->onlyOnForms(),
            Image::make('Photo')->required()->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo')->toMediaCollection('news');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('news', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('news', 'thumbnail');
            })->hideFromIndex()->deletable(false),
            Select::make('Status')->required()->options($statuses)->sortable(),
            Quill::make('Content')->withFiles()
            ->toolbar([
                [[ 'header' => 2 ]],               
                ['bold', 'italic', 'underline'],
                [['list' => 'ordered'], ['list' => 'bullet']],
                ['blockquote'],
                ['link', 'image', 'video'],
                ['clean'],
            ])->required()->withFiles('public'),
            Textarea::make('Meta Description','meta_description')->onlyOnForms(),
            Textarea::make('Keywords','keywords')->onlyOnForms(),
            BelongsTo::make('User'),
            BelongsToMany::make('Categories'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
