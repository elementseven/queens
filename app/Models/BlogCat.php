<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BlogCat extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','slug'
    ];
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($category) {
            $category->slug = Str::slug($category->name, "-");
        });
    }

    public function privateblogs(){
        return $this->belongsToMany('App\Models\PrivateBlog', 'blog_cat_private_blog')->withPivot('private_blog_id');
    }
}
