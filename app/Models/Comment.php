<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'status',
        'content',
        'private_blog_id',
        'public_blog_id',
        'user_id',
    ];

    public function public_blog(){
        return $this->belongsTo('App\Models\PublicBlog');
    }

    public function private_blog(){
        return $this->belongsTo('App\Models\PrivateBlog');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}