<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Support extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    protected $fillable = [
      'title',
      'excerpt',
      'meta_description',
      'keywords',
      'content',
      'photo',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($i) {
            $i->slug = Str::slug($i->title, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(600)->crop(600, 391);
        $this->addMediaConversion('normal-webp')->width(600)->format('webp')->crop(600, 391);
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop(600, 267);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('support')->singleFile();
    }

    public function categories(){
        return $this->belongsToMany('App\Models\Category', 'category_support')->withPivot('category_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
