<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Future extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'lead',
        'contact',
        'short_description',
        'extended_description',
        'start_date',
        'expected_duration',
        'participants_needed',
        'participants_requirements',
        'participation_description',
        'remuneration'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'start_date' => 'date'
    ];

}
