<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Document extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
      'name',
      'slug',
      'status',
      'excerpt',
      'link'
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($document) {
            $document->slug = Str::slug($document->name, "-");
        });
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('research-files')->singleFile();
    }

    public function ResourceCategory(){
        return $this->belongsTo('App\Models\DocumentCategory');
    }

    public function documentCategory(){
        return $this->belongsTo('App\Models\DocumentCategory');
    }

}   
