<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class CaseStudy extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'slug',
        'image',
        'file',
        'status'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($casestudy) {
            $casestudy->slug = Str::slug($casestudy->title, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
        'start_date' => 'date'
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(595)->crop(595, 842);
        $this->addMediaConversion('normal-webp')->width(595)->format('webp')->crop(595, 842);
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop(595, 842);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('case_studies')->singleFile();
        $this->addMediaCollection('case_study_pdfs')->singleFile();
    }
}