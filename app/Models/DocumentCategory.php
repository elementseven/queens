<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
    use HasFactory;

    protected $fillable = [
      'name',
      'slug',
      'colour'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($documentCategory) {
            $documentCategory->slug = Str::slug($documentCategory->name, "-");
        });
    }

    public function documents(){
        return $this->hasMany('App\Models\Document');
    }

}
