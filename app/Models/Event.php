<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
      'title',
      'slug',
      'excerpt',
      'meta_description',
      'keywords',
      'status',
      'description',
      'datetime',
      'link',
    ];

    protected $casts = [
        'datetime' => 'datetime',
        'created_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($event) {
            $event->slug = Str::slug($event->title, "-");
        });
    }

}   
