/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("babel-polyfill");
require("whatwg-fetch");
require('./bootstrap');
window.LazyLoad = require('vanilla-lazyload');
import Vue from 'vue';
require('./plugins/cookieConsent.js');
require('./plugins/modernizr-custom.js');
require('waypoints/lib/jquery.waypoints.min.js');

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import vueDebounce from 'vue-debounce';
Vue.use(vueDebounce, {
  listenTo: ['input', 'keyup']
});
window.AOS = require('AOS');
AOS.init({ offset: (window.innerHeight * 0) });
window.addEventListener('load', AOS.refresh);
window.addEventListener('resize', AOS.refresh);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

Vue.component('main-menu', () => import(/* webpackChunkName: "MainMenu" */ './components/Menus/MainMenu.vue'));
Vue.component('mobile-menu', () => import(/* webpackChunkName: "MobileMenu" */ './components/Menus/MobileMenu.vue'));
Vue.component('site-footer', () => import(/* webpackChunkName: "Footer" */ './components/Footer.vue'));
Vue.component('footer-topper', () => import(/* webpackChunkName: "FooterTopper" */ './components/FooterTopper.vue'));
Vue.component('footer-mailing-list', () => import(/* webpackChunkName: "FooterMailingList" */ './components/FooterMailingList.vue'));

Vue.component('mailing-list', () => import(/* webpackChunkName: "MailingList" */ './components/MailingList.vue'));
Vue.component('loader', () => import(/* webpackChunkName: "Loader" */ './components/Loader.vue'));

Vue.component('contact-page-form', () => import(/* webpackChunkName: "ContactPageForm" */ './components/Contact/ContactPageForm.vue'));
Vue.component('get-involved-form', () => import(/* webpackChunkName: "GetInvolvedForm" */ './components/GetInvolvedForm.vue'));

Vue.component('home-header', require(/* webpackChunkName: "HomeHeader" */ './components/Home/HomeHeader.vue').default);
Vue.component('link-boxes', () => import(/* webpackChunkName: "LinkBoxes" */ './components/Home/LinkBoxes.vue'));
Vue.component('home-search', () => import(/* webpackChunkName: "HomeSearch" */ './components/Home/HomeSearch.vue'));
Vue.component('home-case-studies', () => import(/* webpackChunkName: "HomeCaseStudies" */ './components/Home/HomeCaseStudies.vue'));

Vue.component('news-index', () => import(/* webpackChunkName: "NewsIndex" */ './components/news/Index.vue'));
Vue.component('news-show', () => import(/* webpackChunkName: "NewsShow" */ './components/news/Show.vue'));
Vue.component('news-inline', () => import(/* webpackChunkName: "NewsShow" */ './components/news/Inline.vue'));

Vue.component('support-index', () => import(/* webpackChunkName: "SupportIndex" */ './components/support/Index.vue'));
Vue.component('support-show', () => import(/* webpackChunkName: "SupportShow" */ './components/support/Show.vue'));
Vue.component('support-inline', () => import(/* webpackChunkName: "SupportShow" */ './components/support/Inline.vue'));


Vue.component('events-index', () => import(/* webpackChunkName: "EventsIndex" */ './components/Events/Index.vue'));
Vue.component('events-show', () => import(/* webpackChunkName: "EventsShow" */ './components/Events/Show.vue'));

Vue.component('case-studies-index', () => import(/* webpackChunkName: "CaseStudiesIndex" */ './components/CaseStudies/Index.vue'));
Vue.component('case-studies-show', () => import(/* webpackChunkName: "CaseStudiesShow" */ './components/CaseStudies/Show.vue'));

Vue.component('resources-index', () => import(/* webpackChunkName: "ResourcesIndex" */ './components/Resources/Index.vue'));
Vue.component('resources-show', () => import(/* webpackChunkName: "ResourcesShow" */ './components/Resources/Show.vue'));

Vue.component('seen-enough', () => import(/* webpackChunkName: "SeenEnough" */ './components/contact/SeenEnough.vue'));

Vue.component('users-main-menu', () => import(/* webpackChunkName: "UsersMainMenu" */ './components/Menus/UsersMainMenu.vue'));
Vue.component('users-mobile-menu', () => import(/* webpackChunkName: "UsersMobileMenu" */ './components/Menus/UsersMobileMenu.vue'));

Vue.component('private-blog-index', () => import(/* webpackChunkName: "PrivateBlogIndex" */ './components/PrivateBlog/Index.vue'));
Vue.component('private-blog-show', () => import(/* webpackChunkName: "PrivateBlogShow" */ './components/PrivateBlog/Show.vue'));


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#app',
});
