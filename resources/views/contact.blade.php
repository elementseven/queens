@php
$page = 'Contact';
$pagetitle = 'Contact | QUB PPI Hub';
$metadescription = "For more information about the PPI Network at Queen’s University contact us today.";
$pagetype = 'dark';
$pagename = 'contact';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mb-3 mob-pb-0 mt-5">
	<div class="row mt-5 pt-5 mob-pt-0 mob-mt-0">
		<div class="col-lg-8">
			<h1>Contact</h1>
			<p class="">For more information about the PPI Network at Queen’s University contact <a href="mailto:ppihub@qub.ac.uk" class="text-primary"><u>ppihub@qub.ac.uk</u></a>, or use the form below.</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8 mb-5">
      <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
  	</div>
  	<div class="col-lg-4 mb-5 pl-5 mob-px-3">
      <p class="text-small"><b>QUB PPI Network,</b><br/>Research Strategy & Engagement Team,<br/>Research & Enterprise Directorate,<br/>63 University Road,<br/>University Road, Belfast,<br/>Northern Ireland, BT7 1NN</p>      
      {{-- <p class="mb-4"><a href="tel:000123456789" class=" text-dark"><span class="text-primary"><i class="fa fa-phone mr-2"></i></span> +44 (0)28 9024 5133</a></p> --}}
      <ul class="social-links-circled">
        <li><a class="twitter" href="https://x.com/QUBelfast" target="_blank" rel="noreferrer" aria-label="Links - Twitter"><i class="fa-brands fa-x-twitter"></i></a></li>
      </ul>
    </div>
	</div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection