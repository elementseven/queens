@php
$page = 'News';
$pagetitle = 'Latest News | QUB PPI Hub';
$metadescription = 'Please check this page regularly for updates on PPI theory and practice. We will share perspectives on local, national and international best practice in PPI.';
$pagetype = 'dark';
$pagename = 'news';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container position-relative top-padding mb-4">
	<div class="py-lg-5">
		<h1 class="text-uppercase">PPI News</h1>
		<p>Here we celebrate PPI activity across the QUB PPI Network, and further afield, through reporting of PPI activities and events that have taken place. We also share perspectives on local, national and international best practice in PPI.</p>
	</div>
</header>

@endsection
@section('content')
<news-index :categories="{{$categories}}"></news-index>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection