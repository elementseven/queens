@php
$page = 'About';
$pagetitle = 'About | QUB PPI Hub';
$metadescription = "Private blog";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.users', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0">
    <div class="row mt-5 pt-5">
        <div class="col-12 text-center text-lg-left">
            <div class="position-relative z-2">
                <h1 class="mb-4">Private Blog</h1>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<private-blog-index :categories="{{$categories}}"></private-blog-index>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection