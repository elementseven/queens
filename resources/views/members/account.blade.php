@php
$page = 'Account';
$pagetitle = 'Account - Manage your QUB PPI Account';
$metadescription = "Manage your QUB PPI Account";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.users', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0">
    <div class="row mt-5 pt-5">
        <div class="col-12 text-center text-lg-left">
            <div class="position-relative z-2">
                <h1 class="mb-4">Account</h1>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
@if ($errors->any())
<div class="container">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>Attention</strong> there was an error changing your details, check the form below.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
</div>
@endif
@if(session('action_status'))
<div class="container">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>{{session('action_status')}}</strong> {{session('action_message')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
</div>
@endif
@php 
session()->forget('action_status');
session()->forget('action_message');
@endphp
<div class="container">
    <div class="row py-5">
        <div class="col-md-12">
            <h3 class="">Your Details</h3>
            <hr class="primary_line">
        </div>
        <div class="col-md-8">
            <p class=" mb-3">Only the information you provided at registration is stored. Your details are kept in a secure database so you may log in and use this system again in the future. If you wish to have your details removed from our system please contact us.</p>
        </div>
        <div class="col-sm-6">
            <p class="marg_top_20"><b>Name: </b>{{$currentUser->first_name}} {{$currentUser->last_name}}</p>
            <p class="marg_top_20"><b>Email: </b>{{$currentUser->email}}</p>
        </div>
        <div class="col-sm-6">
            <p class="marg_top_20"><b>Registered since: </b>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($currentUser->created_at))->format('d/m/Y') }}</p>
            <p class="marg_top_20"><b>Details last updated: </b>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($currentUser->updated_at))->format('d/m/Y') }}</p>
        </div>
    </div>
    <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Change Details</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">Change your name using the form below. Note that after saving the changes, these will be the details we use to contact you.</p>
                </div>
            </div>
            <form action="{{route('update-account')}}" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-6 half_col mb-3">
                        <label class="mb-0" for="first_name"><b>First Name*</b></label>
                        <input name="first_name" type="text" class="form-control" placeholder="First Name" value="{{$currentUser->first_name}}"/>
                        @if ($errors->has('first_name'))
                            <span class="help-block text-primary">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-3">
                        <label class="mb-0" for="last_name"><b>Last Name*</b></label>
                        <input name="last_name" type="text" class="form-control" placeholder="Last Name" value="{{$currentUser->last_name}}"/>
                        @if ($errors->has('last_name'))
                            <span class="help-block text-primary">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-12 half_col mb-3">
                        <button type="submit" class="btn btn-primary float-right mt-0 px-5">Confirm</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Change Password</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">Change your password using the form below. Note that after saving your new password, you will need to use it to log in afterwards.</p>
                </div>
            </div>
            <form action="/change-password" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-4 half_col mb-3">
                        <label class="mb-0" for="current_password"><b>Current Password*</b></label>
                        <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Current Password" required>
                        @if ($errors->has('current_password'))
                            <span class="help-block text-primary">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4 half_col mb-3">
                        <label class="mb-0" for="password"><b>New Password*</b></label>
                        <input id="password" type="password" class="form-control" name="password" placeholder="New Password" required>
                        @if ($errors->has('password'))
                            <span class="help-block text-primary">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4 half_col mb-3">
                        <label class="mb-0" for="password-confirm"><b>Confirm New Password*</b></label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm New Password" required>
                    </div>
                    <div class="col-md-12 half_col">
                        <button type="submit" class="btn btn-primary float-right mt-0">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Remove Account</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">You can remove your account and all of your information from this web app. This action is irreversible and you will lose access to members only content. You can create a new account afterwards.</p>
                </div>
            </div>
            <form action="/remove-account" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-4 half_col mb-3">
                        <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Current Password" required>
                        @if ($errors->has('current_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-8 half_col">
                        <button type="submit" class="btn btn-primary float-right mt-0">Remove Account</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection