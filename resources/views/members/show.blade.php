@php
$page = 'News';
$pagetitle = $post->title . ' | QUB PPI Hub';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'dark';
$pagename = 'news';
$ogimage = 'https://ppihub.org' . $post->getFirstMediaUrl('normal');
@endphp
@extends('layouts.users', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding mb-5 mob-mb-0">
  <div class="row mt-5 pt-5">
    <div class="container">
      <div class="row justify-content-center pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-12 pt-5 ipadp-pt-0 mob-pt-0 mob-px-4 mob-mb-3">
          <p class="text-title"><a href="{{route('members.private-blog')}}" class=" text-primary"><img src="/img/icons/arrow-red-short-left.svg" class="mr-2"/><b>Back to private blog</b></a></p>
          <h1 class="mb-3 blog-title">{{$post->title}}</h1>
          <p class="text-light-grey text-title mb-0">by {{$post->user->full_name}} - {{\Carbon\Carbon::parse($post->created_at)->format('jS M Y')}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 text-left">
        <div class="col-12 mob-mt-0 blog-body">
          {!!$post->content!!}
        </div>
    </div>
</div>
@if(count($others))
<div class="container-fluid bg-primary-lower-half">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p class="mimic-h2 mb-4">Related Articles</p>
        </div>
        @foreach($others as $other)
        <div class="col-lg-4">
          <a href="/members/private-blog/{{\Carbon\Carbon::parse($other->created_at)->format('Y-m-d')}}/{{$other->slug}}">
            <div class="card transition post-card border-radius-0 text-dark px-0 py-0 overflow-hidden">
              <picture>
                <source srcset="{{$other->getFirstMediaUrl('private_blogs', 'normal-webp')}}" type="image/webp"/> 
                <source srcset="{{$other->getFirstMediaUrl('private_blogs', 'normal')}}" type="post.mimetype"/> 
                <img src="{{$other->getFirstMediaUrl('private_blogs', 'normal')}}" type="{{$other->getFirstMedia('private_blogs')->mime_type}}" alt="{{$other->title}}" class="lazy w-100 h-auto" width="460" height="322" />
              </picture>
              <div class="post-box-info p-3 pt-4 bg-white">
                <p class="text-smallest text-title text-primary text-uppercase letter-spacing mb-1"><b>{{\Carbon\Carbon::parse($other->created_at)->format('jS M Y')}}</b></p>
                <p class="mb-2 post-box-title"><b>{{substr($other->title,0,36)}}...</b></p>
                <p class="mb-3 text-small">{{substr($other->excerpt,0,80)}}...</p>
                <p class="mb-0 post-box-read text-primary"><b>Read Article</b> <img src="/img/icons/arrow-red-short.svg" alt="read news article arrow" width="27" height="17" class="post-box-read-arrow transition" /></p>
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endif
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
        $('.fr-video iframe').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection