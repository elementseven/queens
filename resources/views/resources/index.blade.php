@php
$page = 'Resources';
$pagetitle = "Resource Hub | QUB PPI Hub";
$metadescription = "This page acts as a repository for a range of different PPI-related resources from local, national and international sources. If you would like to share PPI resources on the PPI hub that you have developed, please contact us.";
$pagetype = 'light';
$pagename = 'resources';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container position-relative top-padding mb-4">
	<div class="py-lg-5">
		<h1 class="text-uppercase">Resource Hub</h1>
		<p>This page acts as a repository for a range of useful resources to guide best-practice in PPI in education and research, from local, national and international sources. If you would like to share PPI resources that you have developed, please contact us.</p>
	</div>
</header>
@endsection
@section('content')
<resources-index></resources-index>
{{-- <div class="container py-5">
	<div class="row">
		<div class="col-12">
			<p class="mimic-h2">Useful Links for PPI Resources</p>
			<p class="mb-0"><b>PPI Ignite Network</b></p>
			<p class="mb-4"><a href="https://ppinetwork.ie/" class="text-primary" target="_blank"><u>PPI Ignite Network • Promoting excellence and innovation in PPI (ppinetwork.ie)</u></a>
			<p class="mb-0"><b>UK Standards for Public Involvement in Research website</b></p>
			<p class="mb-4"><a href="https://sites.google.com/nihr.ac.uk/pi-standards/home" class="text-primary" target="_blank"><u>https://sites.google.com/nihr.ac.uk/pi-standards/home</u></a>
			<p class="mb-0"><b>National Institute for Health Research</b></p>
			<p class="mb-4"><a href="https://www.nihr.ac.uk/health-and-care-professionals/engagement-and-participation-in-research/involve-patients.htm" class="text-primary" target="_blank"><u>Involve patients | NIHR</u></a>
			<p class="mb-0"><b>Patient Centred Outcomes Research Institute</b></p>
			<p class="mb-4"><a href="https://www.pcori.org/" class="text-primary" target="_blank"><u>Homepage | PCORI</u></a>
			<p class="mb-0"><b>Glossary CF Europe</b><br/>A plain language glossary of terms often used in clinical trials of treatments for cystic fibrosis</p>
			<p class="mb-4"><a href="https://www.cf-europe.eu/what-we-do/glossary/" class="text-primary" target="_blank"><u>Glossary - CF Europe (cf-europe.eu)</u></a>
		</div>
	</div>
</div> --}}
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection