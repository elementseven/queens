@php
$page = 'About';
$pagetitle = 'About | QUB PPI Hub';
$metadescription = "The Queen’s University Personal and Public Involvement in Research (PPI) Network was established in 2020 as part of the Engaged Research Action Plan. The aim of the network is to support the excellent PPI ongoing across the university by providing opportunities to build capacity in PPI, to network and share resources and best practice.";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0 mob-pt-0">
	<div class="row mt-5 pt-5">
		<div class="col-12">
		    <div class="position-relative z-2">
				<h1 class="mb-4">PPI Network at Queen’s</h1>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5 pb-5 mob-pb-0">
	<div class="row">
		<div class="col-lg-10">
			<p class="">The Queen’s University Belfast (QUB) Personal and Public Involvement (PPI) Network was established in 2020 as part of the Engaged Research Action Plan.</p>
			<p>The QUB PPI Network has members, representing a variety of education and research disciplines, from each of the three university Faculties of Arts, Humanities and Social Sciences, Engineering and Physical Sciences and Medicine, Health and Life Sciences. Importantly, the QUB PPI Network membership also includes PPI contributors, researchers at all stage of their respective research careers, colleagues from Ulster University, the Public Health Agency (PHA) and other external stakeholders from patient advocacy groups, charitable organisations and Health and Social Care Trusts across Northern Ireland.</p>
			<p class="mimic-h3 mt-5 pt-lg-5">PPI Ignite Network</p>
			<p>Queen’s University Belfast is a National Partner in the all-Ireland PPI Ignite Network.The PPI Ignite Network aims to provide a shared voice for PPI across Ireland, aiming to change the research culture, an important contributor to improving health outcomes for the public.</p>
			<p>The PPI Ignite Network includes seven Universities in Ireland as members, with a centralised National Programme Office. There are ten national partners and fifty-three local partners.</p>
			<p>The PPI Ignite Network is funded by the Health Research Board (HRB) and Taighde Éireann - Research Ireland, with co-funding from each of the Lead Universities, and builds on the initial PPI Ignite Programme (2017-2021).</p>
			<p>Find more information on the PPI Ignite Network <a href="https://ppinetwork.ie/" target="_blank" class="text-primary"><u>here</u></a>.</p>
		</div>
	</div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection