@php
$page = 'Get Involved';
$pagetitle = 'Get Involved | QUB PPI Hub';
$metadescription = "Please get in touch with us if you are currently involved in any aspect of PPI research; if you would like to suggest a PPI case study for inclusion on the website or if you would like further information about ongoing PPI research at QUB. We would love to hear from you.";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0 mob-pt-0">
	<div class="row mt-5 pt-5">
		<div class="col-12">
		    <div class="position-relative z-2">
				<h1 class="mb-4">Get Involved</h1>
				<p>Please get in touch with us if you are currently involved in any aspect of PPI research; if you would like to suggest a PPI case study for inclusion on the website or if you would like further information about ongoing PPI research at QUB. We would love to hear from you.</p>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5 pb-5">
	<div class="row">
		<div class="col-lg-10 mb-4">
{{-- 			<p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat.</p> --}}
		</div>
		<div class="col-lg-12">
			<get-involved-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></get-involved-form>
		</div>
	</div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection