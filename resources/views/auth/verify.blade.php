@php
$page = 'Verfiy Email Address';
$pagetitle = "Verfiy Email Address - PPI Hub Queen's University Belfast";
$metadescription = "Verfiy the Email Address for your account";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://lorem.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0">
    <div class="row mt-5 pt-5">
        <div class="col-12 text-center text-lg-left">
            <div class="position-relative z-2">
                <h1 class="mb-2">Verfiy Email Address</h1>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5">
    <div class="row">
        <div class="col-md-8">

            @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    {{ __('A fresh verification link has been sent to your email address.') }}
                </div>
            @endif

            <p>Please check your email for a verification link, remember to check your junk folder. If you did not receive the email you can request another one using the button below.</p>
            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit" class="btn btn-primary p-0 m-0 align-baseline d-inline">{{ __('Request verification link') }}</button>
            </form>
       
        </div>
    </div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection
