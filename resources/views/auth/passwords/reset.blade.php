@php
$page = 'Reset Password';
$pagetitle = "Reset Password - PPI Hub Queen's University Belfast";
$metadescription = "Reset password for your account";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://lorem.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0">
    <div class="row mt-5 pt-5">
        <div class="col-12 text-center text-lg-left">
            <div class="position-relative z-2">
                <h1 class="mb-4">Reset Password</h1>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5">
    <div class="row">
        <div class="col-lg-6">
            <form method="POST" action="{{ route('password.update') }}" class="row">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                

                <div class="col-12 mb-2">
                    <label for="email" class="col-form-label text-md-right"><b>{{ __('E-Mail Address') }}</b></label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Email Address">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                

                <div class="col-12 mb-2">
                    <label for="password" class="col-form-label text-md-right"><b>{{ __('Password') }}</b></label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="New Password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                

                <div class="col-12 mb-4">
                    <label for="password-confirm" class="col-form-label text-md-right"><b>{{ __('Confirm New Password') }}</b></label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm New Password">
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Reset Password') }}
                    </button>
                </div>

            </form>

        </div>
    </div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection
