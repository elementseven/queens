@php
$page = 'Confirm Password';
$pagetitle = "Confirm Password - PPI Hub Queen's University Belfast";
$metadescription = "Confirm the password for your account";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://lorem.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0">
    <div class="row mt-5 pt-5">
        <div class="col-12 text-center text-lg-left">
            <div class="position-relative z-2">
                <h1 class="mb-2">Confirm Password</h1>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5">
    <div class="row">
        <div class="col-lg-6">

            <p>Please confirm your password before continuing.</p>

            <form method="POST" action="{{ route('password.confirm') }}" class="row">
                @csrf

                

                <div class="col-12 mb-4">
                    <label for="password" class="col-form-label text-md-right"><b>{{ __('Password') }}</b></label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Confirm Password') }}
                    </button>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </div>
            </form>

        </div>
    </div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection
