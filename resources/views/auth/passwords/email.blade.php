@php
$page = 'Reset Password';
$pagetitle = "Reset Password - PPI Hub Queen's University Belfast";
$metadescription = "Reset password for your account";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://lorem.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0">
    <div class="row mt-5 pt-5">
        <div class="col-12 text-center text-lg-left">
            <div class="position-relative z-2">
                <h1 class="mb-4">Reset Password</h1>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5">
    <div class="row">
        <div class="col-lg-6">

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}" class="row">
                @csrf
                <div class="col-12 mb-3">
                    <label for="email" class="col-form-label text-md-right"><b>{{ __('E-Mail Address') }}</b></label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email Address" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection
