@php
$page = 'Register';
$pagetitle = "Register - PPI Hub Queen's University Belfast";
$metadescription = "Register your account";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://lorem.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])

@section('header')
<header class="container pt-5 mob-pb-0 mt-5 mob-mt-0">
    <div class="row mt-5 pt-5">
        <div class="col-12 text-center text-lg-left">
            <div class="position-relative z-2">
                <h1 class="mb-4">Register</h1>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5 ">
    <div class="row justify-content-center">
        <div class="col-12">

            <form method="POST" action="{{ route('register') }}" class="row">
                
                <div class="col-md-6 mb-2">
                    @csrf
                    <label for="first_name" class="col-form-label text-md-right mb-0"><b>{{ __('First Name') }}</b></label>
                    <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" required autocomplete="first_name" autofocus>

                    @error('first_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6 mb-2">
                    <label for="last_name" class="col-form-label text-md-right mb-0"><b>{{ __('Last Name') }}</b></label>
                    <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name" required autocomplete="last_name" autofocus>

                    @error('last_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-12 mb-2">
                    <label for="email" class="col-form-label text-md-right mb-0"><b>Email Address</b> - <span class="text-smaller">must be a valid qub.ac.uk email address</span></label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email Address">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6 mb-2">
                    <label for="password" class="col-form-label text-md-right mb-0"><b>{{ __('Password') }}</b></label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6 mb-2">
                    <label for="password-confirm" class="col-form-label text-md-right mb-0"><b>{{ __('Confirm Password') }}</b></label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                </div>
                <div class="col-12 text-right mt-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection
