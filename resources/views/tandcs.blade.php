@php
$page = 'Terms & Conditions';
$pagetitle = 'Terms & Conditions - Lorem ipsum';
$metadescription = "Read our Terms & Conditions";
$pagetype = 'dark';
$pagename = 'terms-and-conditions';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5 mob-px-4">
	<div class="row mt-5 pt-5 mob-pt-0">
		<div class="col-12 text-left">
		    <div class="position-relative z-2">
				<h1>Terms & Conditions</h1>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
	<div class="row">
		<div class="col-lg-10">			
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.</p>
		</div>
	</div>
</div>
<seen-enough title='Interested in this?' sentence="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection