@php
$page = 'Event';
$pagetitle = $event->title . ' | QUB PPI Hub';
$metadescription = $event->meta_description;
$keywords = $event->keywords;
$pagetype = 'dark';
$pagename = 'event';
$ogimage = 'https://ppihub.org/im/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding mb-5 mob-mb-0">
  <div class="row mt-5 pt-5">
    <div class="container">
      <div class="row justify-content-center pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-12 pt-5 ipadp-pt-0 mob-pt-0 mob-px-4 mob-mb-3">
          <p class="text-title"><a href="{{route('events.index')}}" class=" text-primary"><img src="/img/icons/arrow-red-short-left.svg" class="mr-2"/><b>Back to browse events</b></a></p>
          <h1 class="mb-3 -title">{{$event->title}}</h1>
          <p class="text-primary text-title text-uppercase mb-4"><i class="fa fa-calendar-o mr-2"></i>{{\Carbon\Carbon::parse($event->datetime)->format('l jS M Y')}} <i class="fa fa-map-marker ml-4 mr-2 mob-ml-0"></i> {{$event->location}}</p>
          @if($event->link)
          <a href="{{$event->link}}" target="_blank">
            <button type="button" class="btn btn-primary">Attend this event</button>
          </a>
          @endif
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 text-left">
        <div class="col-12 mob-mt-0 -body">
          {!!$event->description!!}
        </div>
        
        <div class="col-lg-4 mt-5">
          <p class="mb-1 text-large"><b>Share this  event:</b>
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook ml-3"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($event->title)}}&amp;summary={{urlencode($event->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($event->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($event->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
        <div class="col-lg-3 mt-5 mob-mt-3">
          @if($event->link)
          <a href="{{$event->link}}" target="_blank">
            <button type="button" class="btn btn-primary">Attend this event</button>
          </a>
          @endif
        </div>
    </div>
</div>

<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
        $('.fr-video iframe').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection