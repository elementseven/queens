@php
$page = 'Events';
$pagetitle = 'Events - QUB PPI Hub';
$metadescription = 'Please check this page regularly for updates on PPI events taking place within QUB; across Ireland via the PPI Ignite Network and beyond.';
$pagetype = 'dark';
$pagename = 'events';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container position-relative top-padding mb-4">
	<div class="py-lg-5">
		<h1 class="text-uppercase">Events</h1>
		<p>Please check this page regularly for updates on PPI events taking place within QUB; across Ireland via the PPI Ignite Network and beyond.</p>
	</div>
</header>

@endsection
@section('content')
<events-index></events-index>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection