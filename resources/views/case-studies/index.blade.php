@php
$page = 'Case Studies';
$pagetitle = 'Case Studies | QUB PPI Hub';
$metadescription = 'There are numerous excellent examples of PPI research being undertaken across QUB. Here we share some examples of this work, highlighting the collaborative working relationships between our PPI contributors and researchers';
$pagetype = 'dark';
$pagename = 'case-studies';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container position-relative top-padding mb-4">
	<div class="py-lg-5">
		<h1 class="text-uppercase">Case Studies</h1>
		<p>There are numerous excellent examples of PPI research being undertaken across QUB. Here we share some examples of this work, highlighting the collaborative working relationships between our PPI contributors and researchers.</p>
	</div>
</header>

@endsection
@section('content')
<case-studies-index></case-studies-index>
<div class="container py-5">
	<div class="row">
		<div class="col-12">
			<p class="mimic-h2">QUB PPI Seed Fund</p>
			<p>The QUB PPI Seed Fund fosters and supports PPI across the university. This funding targets the establishment of PPI within a research team and the development of collaborative partnerships with PPI research contributors. Funds will be allocated through an application process, with application forms available on request from <a class="text-primary" href="mailto:ppihub@qub.ac.uk">ppihub@qub.ac.uk</a>. Amounts of up to £500 are available.</p>
		</div>
	</div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection