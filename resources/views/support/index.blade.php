@php
$page = 'Support';
$pagetitle = 'QUB PPI Network Funding Support | QUB PPI Hub';
$metadescription = 'Reports from PPI events and activities that have received funding from the QUB PPI Network are available to read on this page.';
$pagetype = 'dark';
$pagename = 'support';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container position-relative top-padding mb-4">
	<div class="py-lg-5">
		<h1 class="text-uppercase mb-4">QUB PPI Network <br class="d-none d-md-block"/>Funding Support</h1>
		<p>The QUB PPI Network offers seed funding support to colleagues across QUB undertaking PPI events and activities. This funding targets the establishment or maintenance of PPI initiatives within education or research teams and the development of sustained, collaborative partnerships with PPI contributors. These internal funds are allocated through <a href="https://qubstudentcloud.sharepoint.com/sites/int-re/SitePages/Internal-Funding.aspx#queen-s-university-agility-fund" target="_blank">Agility Fund, strand (iv)</a> “Support for research engagement and impact”</a>.</p>
		<p>Reports from PPI events and activities that have received funding from the QUB PPI Network are available to read below.</p>
	</div>
</header>
@endsection
@section('content')
<support-index :categories="{{$categories}}"></support-index>
<div class="container pb-5">
	<div class="row py-lg-5">
		<div class="col-12">
			<p class="text-large">For further support embedding PPI into teaching and research practices, please consider visiting the following websites:</p>
			<ul class="mb-5">
				<li><a href="https://engage.hscni.net/" target="_blank">HSC Engage</a></li>
				<li><a href="https://acmedsci.ac.uk/more/news/supporting-patient-involvement-in-research-through-new-fund" target="_blank">The Academy of Medical Sciences</a></li>
				<li><a href="https://ppinetwork.ie/" target="_blank">PPI Ignite Network</a></li>
				<li><a href="https://sites.google.com/nihr.ac.uk/pi-standards/home" target="_blank">NIHR UK Standards for Public Involvement</a></li>
			</ul>
			<p>New and emerging resources to support PPI practice will be actively communicated via the monthly QUB PPI Network bulletin, so please do join our Network for more up to date information.</p>
		</div>
	</div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection	