@php
$page = 'Homepage';
$pagetitle = "Queen's University Belfast - Personal & Public Involvement Hub";
$metadescription = "Personal and Public Involvement (PPI) means involving people in all aspects of the research process as partners rather than as research contributors. This involvement might take place at all or any of the stages of the research process.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://ppihub.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<home-header></home-header>
@endsection
@section('content')

<div class="container py-5 mt-4 mob-mt-0">
    <div class="row mb-5 mob-mb-0">
        <div class="col-lg-12">
            <p class="mb-4"><b>Personal and Public Involvement</b> (PPI) means involving people in all aspects of research and education as partners, rather than as participants. Involvement opportunities can manifest in multiple forms, for example:</p>
            <ul class="mb-4">
                <li><p class="mb-0">Design and prioritisation of research questions</p></li>
                <li><p class="mb-0">Co-design of research/educational resources e.g. co-creation of research programmes, curriculum development</p></li>
                <li><p class="mb-0">Preparation and evaluation of research or educational materials</p></li>
                <li><p class="mb-0">Dissemination of research findings</p></li>
                <li><p class="mb-0">Implementation of research findings</p></li>
                <li><p class="mb-0">Delivery and facilitation of lectures, tutorials, seminars etc and student assessments</p></li>
                <li><p class="mb-0">Participation in lived experience advisory panels and interview panels</p></li>
            </ul>
            <p class="mb-4">The above is not an exhaustive list. Incorporating PPI into academic research and education pathways improves the quality of outputs by placing a focus on real world experience and needs. 
            </p>
            <p class="mb-4">The PPI Network at Queen’s University Belfast aims to support the excellent PPI ongoing across the university by providing opportunities to build capacity in PPI, to network and share resources and best practice.
            </p>
            <a href="/about">
                <button type="button" class="btn btn-primary">Find out more</button>
            </a>
        </div>
    </div>
</div>
<link-boxes></link-boxes>
<div class="py-5 mob-pt-0"></div>
<div class="container-fluid position-relative my-5 py-5 mob-px-4 bg-primary text-white mob-mb-0">
    <div class="row">
        <div class="container">
            <div class="row my-4 py-5 mob-py-0 text-center text-lg-left">
                <div class="col-lg-7">
                    <p class="mb-0 text-large">Queen’s University Belfast has an abundance of outstanding PPI research being carried out across our 3 faculties and this research drives engagement with many hundreds of PPI contributors and collaborators across Northern Ireland and, indeed, further afield. The Hub is a host for sharing information with researchers and contributors in PPI research at all levels. No matter what your level of involvement you will be able to find guidance and support through this Hub.</p>
                </div>
                <div class="col-lg-5">
                    <p class="text-uppercase btm-right mob-mt-5 mb-0"><a href="/about" class="arrow-link text-white">Learn More <span class="arrow-right"></span></a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<home-case-studies></home-case-studies>
<div class="container container-wide pb-4 my-5 mob-mb-0 mob-mt-0">
    <div class="row">
        <div class="col-lg-8 mb-4 mob-mb-3 pt-2">
            <a href="/news">
                <div class="home-tile-bg zoom-link">
                    <div class="zoom-img">
                        <picture>
                            <source src="/img/home/home-ppi-news.webp" type="image/webp"/>
                            <source src="/img/home/home-ppi-news.jpg" type="image/jpeg"/>
                            <img src="/img/home/home-ppi-news.jpg" type="image/jpeg" width="970" height="420" alt="Queen's University Belfast PPI Hub News Articles"/>
                        </picture>
                    </div>
                    <p class="mimic-h2 text-white">PPI News <span class="home-tile-link ml-2"><span class="text-small text-uppercase text-white">News Articles <span class="arrow-right"></span></span></span></p>
                </div>
            </a>
        </div>
        <div class="col-lg-4 mb-4 mob-mb-3 pt-2">
            @if($post != null)
            <a href="/news/{{Carbon\Carbon::parse($post->created_at)->format('Y-m-d')}}/{{$post->slug}}">
                <div class="home-primary-tile bg-primary text-white">
                    <p class="mb-0 text-small text-uppercase tile-title title letter-spacing">News Article</p>
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell align-middle w-100-h-100">
                            <div class="px-4">
                                <p class="text-small letter-spacing mb-0">{{Carbon\Carbon::parse($post->created_at)->format('jS M Y')}}</p>
                                <p class="mimic-h3">{{substr($post->title,0,44)}}@if(strlen($post->title) > 44)...@endif</p>
                                <p>{{substr($post->excerpt,0,120)}}@if(strlen($post->excerpt) > 120)...@endif</p>
                            </div>
                        </div>
                    </div>
                    <p class="mb-0 bottom-link text-small text-uppercase text-white">Read News Article <span class="arrow-right"></span></p>
                </div>
            </a>
            @else
            <a href="/">
                <div class="home-primary-tile bg-primary text-white">
                    <p class="mb-0 text-small text-uppercase tile-title title letter-spacing">News Article</p>
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell align-middle w-100-h-100">
                            <div class="px-4">
                                <p class="text-small letter-spacing mb-0">DEC 6TH 2021</p>
                                <p class="mimic-h3">PPI Funding Scheme</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Duis autem vel eum iriure.</p>
                            </div>
                        </div>
                    </div>
                    <p class="mb-0 bottom-link text-small text-uppercase text-white">Read News Article <span class="arrow-right"></span></p>
                </div>
            </a>
            @endif
        </div>
     {{--    <div class="col-lg-4 mb-4 pt-2">
            <a href="/blog">
                <div class="home-primary-tile bg-primary text-white">
                    <p class="mb-0 text-small text-uppercase tile-title title letter-spacing">Blog Post</p>
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell align-middle w-100-h-100">
                            <div class="px-4">
                                <p class="text-small letter-spacing mb-0">NOV 29TH 2021</p>
                                <p class="mimic-h3">How to get involved</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Duis autem vel eum iriure.</p>
                            </div>
                        </div>
                    </div>
                    <p class="mb-0 bottom-link text-small text-uppercase text-white">Read Blog Post <span class="arrow-right"></span></p>
                </div>
            </a>
        </div>
        <div class="col-lg-8 mb-4 pt-2">
            <a href="/blog">
                <div class="home-tile-bg zoom-link">
                    <div class="zoom-img">
                        <picture>
                            <source src="/img/home/home-blog-posts.webp" type="image/webp"/>
                            <source src="/img/home/home-blog-posts.jpg" type="image/jpeg"/>
                            <img src="/img/home/home-blog-posts.jpg" type="image/jpeg" width="970" height="420" alt="Queen's University Belfast PPI Hub Blog Posts" />
                        </picture>
                    </div>
                    <p class="mimic-h2 text-white">PPI Blog <span class="home-tile-link ml-2"><span class="text-small text-uppercase text-white">Read More Blog Posts <span class="arrow-right"></span></span></span></p>
                </div>
            </a>
        </div> --}}
        @if($event != null)
        <div class="col-lg-4 mb-4 mob-mb-3 pt-2">
            <a href="/events/{{Carbon\Carbon::parse($event->datetime)->format('Y-m-d')}}/{{$event->slug}}">
                <div class="home-primary-tile bg-primary text-white">
                    <p class="mb-0 text-small text-uppercase tile-title title letter-spacing">Next Event</p>
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell align-middle w-100-h-100">
                            <div class="px-4">
                                <p class="mimic-h3">{{substr($event->title,0,44)}}@if(strlen($event->title) > 44)...@endif</p>
                                <p class="text-small text-uppercase letter-spacing title"><i class="fa fa-calendar"></i> {{Carbon\Carbon::parse($event->datetime)->format('jS M Y')}}</p>
                                <p class="text-small text-uppercase letter-spacing title"><i class="fa fa-map-marker"></i> {{$event->location}}</p>
                                <p>{{substr($event->excerpt,0,120)}}@if(strlen($event->excerpt) > 120)...@endif</p>
                            </div>
                        </div>
                    </div>
                    <p class="mb-0 bottom-link text-small text-uppercase text-white">View Event <span class="arrow-right"></span></p>
                </div>
            </a>  
        </div>
        @endif
        <div class="col-lg-8 mb-4 mob-mb-3 pt-2">
            <a href="/events">
                <div class="home-tile-bg zoom-link">
                    <div class="zoom-img">
                        <picture>
                            <source src="/img/home/home-events.webp" type="image/webp"/>
                            <source src="/img/home/home-events.jpg" type="image/jpeg"/>
                            <img src="/img/home/home-events.jpg" type="image/jpeg" width="970" height="420" alt="Queen's University Belfast PPI Hub Events"/>
                        </picture>
                    </div>
                    <p class="mimic-h2 text-white">PPI Events <span class="home-tile-link ml-2"><span class="text-small text-uppercase text-white">Upcoming Events <span class="arrow-right"></span></span></span></p>
                </div>
            </a>
        </div>
        
    </div>
</div>
<footer-mailing-list></footer-mailing-list>
<footer-topper></footer-topper>
@endsection
@section('scripts')

@endsection