<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('futures', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('status');
            $table->string('lead');
            $table->string('contact');
            $table->text('short_description');
            $table->text('extended_description');
            $table->date('start_date')->nullable();
            $table->string('expected_duration')->nullable();
            $table->string('participants_needed')->nullable();
            $table->text('participants_requirements')->nullable();
            $table->string('participation_description')->nullable();
            $table->string('remuneration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('futures');
    }
}
