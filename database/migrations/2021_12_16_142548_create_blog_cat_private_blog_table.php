<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogCatPrivateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_cat_private_blog', function (Blueprint $table) {
            $table->bigInteger('blog_cat_id')->unsigned()->index();
            $table->foreign('blog_cat_id')->references('id')->on('blog_cats')->onDelete('cascade');
            $table->bigInteger('private_blog_id')->unsigned()->index();
            $table->foreign('private_blog_id')->references('id')->on('private_blogs')->onDelete('cascade');
            $table->primary(['blog_cat_id', 'private_blog_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_cat_private_blog');
    }
}
